const express = require('express');
const {getUser, createUser} = require('../controllers/userController.js');

  const router = express.Router();

  router.get('/user', getUser);
  router.post('/user', createUser);
  
  module.exports = router;