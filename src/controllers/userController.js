// This file exports a class UserController which has two methods getUser and createUser.
// getUser handles the GET request to retrieve user data from the user.json file.
// createUser handles the POST request to add a new user to the user.json file.

const fs = require('fs');

  function getUser(req, res) {
    fs.readFile('./src/user.json', 'utf8', (err, data) => {
      if (err) {
        console.error(err);
        res.status(500).send('Internal Server Error');
      } else {
        const users = JSON.parse(data);
        res.status(200).json(users);
      }
    });
  }

  function createUser(req, res) {
    const newUser = req.body;
    fs.readFile('./src/user.json', 'utf8', (err, data) => {
      if (err) {
        console.error(err);
        res.status(500).send('Internal Server Error');
      } else {
        const users = JSON.parse(data);
        users.push(newUser);
        fs.writeFile('./src/user.json', JSON.stringify(users), (err) => {
          if (err) {
            console.error(err);
            res.status(500).send('Internal Server Error');
          } else {
            res.status(201).send('User created successfully');
          }
        });
      }
    });
  }

  module.exports = {
    getUser,
    createUser,
  };
  