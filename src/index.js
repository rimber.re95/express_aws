const express = require('express');
const app = express();
const userRoutes = require('./routes/userRoutes');

app.use(express.json());

app.get('/', (req, res) => {
  res.send('Welcome to our API');
}
);
app.use('/user', userRoutes);

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});

// This file is intentionally left blank.