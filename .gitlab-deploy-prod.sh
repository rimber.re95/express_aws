set -f
string=$IP_PROD
array=(${string//,/ })

for i in "${!array[@]}"; do
  echo "Deploy project on server ${array[i]}"
  ssh ec2-user@${array[i]} "cd /home/ubuntu/express_aws/ && git pull origin master && npm install && pm2 restart all"
done